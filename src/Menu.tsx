import { Link } from 'react-router-dom'

export const Menu = () => {
  return <nav>
    <ul>
      <li><Link to="/home">Home</Link></li>
      <li><Link to="/about">About</Link></li>
      <li><Link to="/projects">Projects</Link></li>
      <li><Link to="/projects/1">Project 1</Link></li>
      <li><Link to="/no-page">No page</Link></li>
    </ul>
  </nav>
}