import { useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

export const Project = () => {
  let { id } = useParams();
  let location = useLocation();
  let navigate = useNavigate();
  
  console.log(location);

  useEffect(() => {
    setTimeout(() => navigate('/'), 3000)
  }, [])

  return <>Project {id}</>;
}