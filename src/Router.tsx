import React from 'react';
import { Route, Routes, useRoutes } from 'react-router-dom';
import { About } from './pages/About';
import { Home } from './pages/Home';
import { Project } from './pages/Project';
import { Projects } from './pages/Projects';

function App() {
  // let element = useRoutes([
  //   { path: 'home', element: <Home /> },
  //   { path: 'about', element: <About />, },
  //   { path: 'projects', 
  //     children: [
  //       { index: true, element: <Projects />},
  //       { path: ":id", element: <Project /> },
  //     ],
  //   },
  //   { path: '*', element: <>404</>, },
  // ]);
  // return element;

  return (
      <Routes>
        <Route index element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="projects">
          <Route index element={<Projects />} />
          <Route path=":id" element={<Project />} />
        </Route>
        <Route path="*" element={<>404</>} />
      </Routes>
  );
}

export default App;
